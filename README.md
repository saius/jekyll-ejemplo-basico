# Repo de prueba de Jekyll

## Tutoriales/repos usados
- [Jekyll - Installation](https://jekyllrb.com/docs/installation/)
- [Jekyll - Step by Step Tutorial](https://jekyllrb.com/docs/step-by-step/01-setup/)
- [Jekyll Tutorial for Beginners](https://blog.webjeda.com/jekyll-guide/)
- [Jekyll Tutorial: How to Create a Static Website](https://www.taniarascia.com/make-a-static-website-with-jekyll/) |
[repo](https://github.com/taniarascia/startjekyll)
- [Compress HTML in Jekyll](https://jch.penibelst.de/) para minificar html

## Comandos usados para inicializar el proyecto
```
# ... instalar ruby, bundle y jekyll
jekyll new jekyll-ejemplo-basico --blank
cd jekyll-ejemplo-basico
bundle add jekyll
git init
git branch -m main
# ... git adds y commits
git remote add origin git@gitlab.com:saius/jekyll-ejemplo-basico.git
git push -u origin main
```

## Uso
Servir con

`bundle exec jekyll serve`
