---
permalink: "/canciones"
title: "Canciones"
---
canciones:

{% for cancion in site.data.canciones %}
  - **{{ cancion.autorx }}** - {{cancion.nombre}}
{% endfor %}
